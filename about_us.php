<?php require_once 'includes/head.php'; ?>
<div class="wapper">
    <?php require_once 'includes/header.php' ?>
    <div class="page_name">
        <div class="container">
            <h2>About Us</h2>
        </div>
    </div>
    <?php require_once 'includes/main_buttons.php'; ?>
    <div class="junk_removal_contant">
        <div class="container">
            <p>Home Builders Services is a Colorado based company specializing in construction roll off services, front load services, temporary dumpster and trash removal services. We have a thoroughly modern fleet of roll off trucks and front load trucks as well as 20 yard, 30 yard and 40 yard roll off dumpsters and other professional grade trash removal equipment.</p>
            <p>Our over 25 years of experience, professional grade equipment and experienced drivers and support staff allow us to provide the highest quality front load, trash removal and temporary roll off dumpster services to our clients.</p>
            <p>When you're in the market for quality, affordable, professional construction debris removal, temporary roll off dumpster or bulk trash removal services delivered on time every time, Home Builder Services is the only company you need to call.</p>
        </div>
    </div>
    <?php require_once 'includes/hbs_add.php' ?>
    <?php require_once 'includes/why_hire_us.php' ?>
    <?php require_once 'includes/footer.php' ?>;
</div>

