<?php
require_once '../includes/head.php';
?>
<div class="wapper">
    <?php require_once '../includes/header.php' ?>
    <div class="page_name">
        <div class="container">
            <h2>Elbert Colorado Dumpster Rental | Roll Off Services</h2>
            <h2>10 yard, 15 yard, 20 yard, 30 yard, 40 yard Trash and Construction Debris Containers</h2>
        </div>
    </div>
    <?php require_once '../includes/main_buttons.php'; ?>
    <div class="junk_removal_contant">
        <div class="container">
            <p>Elbert, CO (ZIP 80106)- Looking for affordable dumpster rentals? Working on a project thats creating a lot of bulk trash or construction debris and need a 20 yard, 30 yard or 40 yard dumpster to throw it into? We can deliver your temporary rental dumpster exactly when and where you need it. Our service is second to none, our dumpster rental pricing is the lowest in Colorado. Give us a call and see why Home Builders Services is the number one dumpster roll off company in Colorado.</p>
            <p>To schedule your temporary dumpster rental please call us at <strong>303-834-3197</strong> or use the "Quote" form at the bottom of this page.</p>
            <p>You load it with construction debris, old junk or bulk trash and we haul it away in one of our 20 yard, 30 yard, or 40 yard dumpsters. Rent by the week, month or more...</p>
        </div>
    </div>
    <?php require_once '../includes/hbs_add.php' ?>
    <div class="yards-block yards-block1">
        <div class="container">
            <ul>
                <li>
                    <img src="../images/HBS-10yard.png">
                    <h4>10 yard</h4>
                    <h4>Front Load Container</h4>
                    <p>13'L X 8'W X 4.5'H</p>
                    <ul>
                        <li>Medium waste removal projects - spring cleanup</li>
                        <li>Kitchen and bathroom remodeling</li>
                        <li>Basement remodeling</li>
                        <li>2 car garage cleaning</li>
                        <li>Deck removal</li>
                        <li>Window replacement</li>
                        <li>Roofing - up to 15 square of shingles</li>
                    </ul>
                </li>
                <li>
                    <img src="../images/HBS-15yard.png">
                    <h4>15 yard</h4>
                    <h4>Front Load Container</h4>
                    <p>13'L X 8'W X 4.5'H</p>
                    <ul>
                        <li>Medium waste removal projects - spring cleanup</li>
                        <li>Kitchen and bathroom remodeling</li>
                        <li>Basement remodeling</li>
                        <li>2 car garage cleaning</li>
                        <li>Deck removal</li>
                        <li>Window replacement</li>
                        <li>Roofing - up to 25 square of shingles</li>
                    </ul>
                </li>
                <li>
                    <img src="../images/HBS-20yard.png">
                    <h4>20 yard</h4>
                    <h4>Front Load Container</h4>
                    <p>13'L X 8'W X 4.5'H</p>
                    <ul>
                        <li>Large waste removal projects - spring cleanup</li>
                        <li>Kitchen and bathroom remodeling</li>
                        <li>Basement remodeling</li>
                        <li>2 car garage cleaning</li>
                        <li>Deck removal</li>
                        <li>Window replacement</li>
                        <li>Siding - garage or small house</li>
                        <li>Roofing - up to 50 square of shingles</li>
                    </ul>
                </li>
                <li>
                    <img src="../images/HBS-30yard.png">
                    <h4>30 yard</h4>
                    <h4>Front Load Container</h4>
                    <p>13'L X 8'W X 4.5'H</p>
                    <ul>
                        <li>Larger trash removal projects</li>
                        <li>Demotion of a one or two car garage</li>
                        <li>Roofing - up to 75 square of shingles</li>
                        <li>Whole house cleanouts with accumulated junk over years</li>
                    </ul>
                </li>
                <li>
                    <img src="../images/HBS-40yard.png">
                    <h4>40 yard</h4>
                    <h4>Front Load Container</h4>
                    <p>13'L X 8'W X 4.5'H</p>
                    <ul>
                        <li>Larger demolition projects</li>
                        <li>Commercial renovation projects</li>
                        <li>Whole house remodels</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="prohibited_items">
        <div class="container">
            <h3>Prohibited Items</h3>
            <ul>
                <li><p><span>NO</span> hazardous waste</p></li>
                <li><p><span>NO</span> liquid waste - freon, chemicals</p></li>
                <li><p><span>NO</span> liquid waste - freon, chemicals</p></li>
                <li><p><span>NO</span> paint (dry cans only)</p></li>
                <li><p><span>NO</span> scrap tires</p></li>
                <li><p><span>NO</span> equipment with gas, oil, or freon</p></li>
                <li><p><span>NO</span> biohazard / medical waste</p></li>
                <li><p><span>NO</span> asbestos or PCB's</p></li>
            </ul>
        </div>
    </div>
    <?php require_once '../includes/why_hire_us.php' ?>
    <div class="junkmap">
        <div class="container">
            <h3>Local Service Area</h3>
            <p>To schedule delivery of your temporary roll off dumpster or front load container in Arapahoe, CO please call 303-834-3197.</p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13602.112635610712!2d74.39300511458737!3d31.537116858315166!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1490618984799" allowfullscreen></iframe>

        </div>
    </div>
    <?php require_once '../includes/footer.php' ?>;
</div>