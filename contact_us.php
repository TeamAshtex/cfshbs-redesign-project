<?php require_once 'includes/head.php'; ?>
<div class="wapper">
    <?php require_once 'includes/header.php' ?>
    <div class="page_name">
        <div class="container">
            <h2>Contact Us</h2>
        </div>
    </div>
    <?php require_once 'includes/main_buttons.php'; ?>
    <div class="junk_removal_contant">
        <div class="container">
            <p>For over 25 years Home Bulders Services has been providing front load, roll off and temporary dumpster rental services. With our fleet of front load and roll off trucks combined with our experienced professionals we can provide the services you need.</p>
            <p>Give us a call and let us help you with the delivery of a 20 yard, 30 yard or 40 yard dumpster to remove your construction debris, bulk trash or old junk. Call us today at 303-834-3197.</p>
        </div>
    </div>
    <?php require_once 'includes/hbs_add.php' ?>
    <div class="form_container">
        <div class="container">
                <h3>Contact via E-mail</h3>
                <div class="input_fields">
                    <input type="text" name="first_name" placeholder="First Name">
                    <input type="text" name="first_name" placeholder="Last Name">
                    <input type="text" name="first_name" placeholder="Email Address">
                    <input type="text" name="first_name" placeholder="Subject">
                    <textarea name="content" placeholder="Message"></textarea>
                    <div class="submit">
                        <button type="submit">Submit</button>
                    </div>
                </div>
        </div>
    </div>
    <div class="dumpers-container">
        <div class="container">
            <div class="main-dumper">
            <img src="images/roll-off-truck-detailed-scalled594.jpg">
        </div>
            <div class="sub-dumpers">
            <img src="images/1roll-off-delivery1.jpg">
            <img src="images/1roll-off-w-dumpster1.jpg">
            <img src="images/1hbs.truck3.index.jpg">
            <img src="images/1hbs.truck.index.jpg">
        </div>
        </div>
    </div>
    <?php require_once 'includes/cities_and_address.php' ?>
    <?php require_once 'includes/footer.php' ?>;
</div>
