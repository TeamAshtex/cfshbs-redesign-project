<div class='footer'>
    <div class='container'>
        <div class='footer-main'>
            <div class='quick-links'>
                <h3>QUICK LINKS</h3>
                <ul>
                    <li><a href='#'>HOME</a></li>
                    <li><a href='#'>ABOUT US</a></li>
                    <li><a href='#'>JUNK REMOVAL</a></li>
                    <li><a href='#'>DUMPSTER SIZES</a></li>
                    <li><a href='#'>CONTACT</a></li>
                    <li><a href='#'>LOGIN</a></li>
                </ul>
            </div>

            <div class='testimonials'>
                <h3>TESTIMONIALS</h3>
                <div class='testimonials-box'>
                    <p>We had no idea how much trash and debris our project would create, we were overwhelmed. We called HBS and spoke with John. He had a dumpster delivered to our house in two days. Thanks for making this the easiest part of our project.</p>
                    <img src='<?php echo BASE_URL; ?>/images/HBS-box-chung_03.png' class='box-nok'>
                </div>
                <h4>Mike Peterson</h4>
            </div>

            <div class='contant-us'>
                <h3>CONTACT US</h3>
                <ul class='contantul1'>
                    <li class='contantli'>3031 Hwy 119</li>
                    <li class='contantli'>Longmont, colorado 80504</li> 
                </ul>
                <ul class='contantul'>
                    <li class='contantli'>Phone: 303-834-3114</li> 
                    <li class='contantli'>Fax: 303-651-5745</li> 
                </ul>
                <ul class='contantul'>
                    <li class='contantli'>Email: hbsdispatch@cofence.com</li> 
                    <li class='contantli'>Website: www.customfencesupply.com</li> 
                </ul>
            </div>
        </div>
    </div>
</div>
<div class='copy-right'>
    <div class='container'>
        <p>Copyright © 2017 All Rights Reserved.</p>
        <ul>
            <li><a href='#'><img src='<?php echo BASE_URL; ?>/images/HBS-facebook.png'></a></li>
            <li><a href='#'><img src='<?php echo BASE_URL; ?>/images/HBS-twitter.png'></a></li>
            <li><a href='#'><img src='<?php echo BASE_URL; ?>/images/HBS-instagram.png'></a></li>
        </ul>
    </div> 
</div>
<div class='popup-file'>
    <div class='container'>
        <div class='popup-bg'></div>
        <div class='popup-main'>
            <div class='popup-content'>
                <a href='javascript:void(0)' class='close_get_a_quote'><img src='<?php echo BASE_URL; ?>/images/close_icon.gif'></a>
                <h4>Helpful info:</h4>
                <p>Need the dumpster for 3 days or less? $25 OFF FOR 3 DAY OR LESS RENTALS</p>
                <p>No electronics or hazardous material please.</p>
                <form>
                    <div class="request-date">
                        <label>Requested delivery date</label>
                        <select name='month'>
                            <option selected='selected' value='-1'>Month</option>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                            <option value='3'>3</option>
                            <option value='4'>4</option>
                            <option value='5'>5</option>
                            <option value='6'>6</option>
                            <option value='7'>7</option>
                            <option value='8'>8</option>
                            <option value='9'>9</option>
                            <option value='10'>10</option>
                            <option value='11'>11</option>
                            <option value='12'>12</option>
                        </select>
                        <select name='day'>
                            <option selected='selected' value='-1'>Day</option>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                            <option value='3'>3</option>
                            <option value='4'>4</option>
                            <option value='5'>5</option>
                            <option value='6'>6</option>
                            <option value='7'>7</option>
                            <option value='8'>8</option>
                            <option value='9'>9</option>
                            <option value='10'>10</option>
                            <option value='11'>11</option>
                            <option value='12'>12</option>
                            <option value='13'>13</option>
                            <option value='14'>14</option>
                            <option value='15'>15</option>
                            <option value='16'>16</option>
                            <option value='17'>17</option>
                            <option value='18'>18</option>
                            <option value='19'>19</option>
                            <option value='20'>20</option>
                            <option value='21'>21</option>
                            <option value='22'>22</option>
                            <option value='23'>23</option>
                            <option value='24'>24</option>
                            <option value='25'>25</option>
                            <option value='26'>26</option>
                            <option value='27'>27</option>
                            <option value='28'>28</option>
                            <option value='29'>29</option>
                            <option value='30'>30</option>
                            <option value='31'>31</option>
                        </select>
                        <select name='year' class='year'>
                            <option selected='selected' value='-1'>Year</option>
                            <option value='1'>2016</option>
                            <option value='2'>2017</option>
                            <option value='3'>2018</option>
                        </select>
                    </div>

                    <div class="inputs">
                        <div class="input-left">
                            <label>Name</label>
                            <input type='text' name='name' placeholder='Name'>
                        </div>
                        <div class="input-right">
                            <label>Phone</label>
                            <input type='text' name='phone' placeholder='Phone (for fastest service)'>
                        </div>
                    </div>

                    <span class="call-check">
                        <h5>Please Call Me</h5>
                        <input type='checkbox' name='vehicle' class='check_box'>
                    </span>

                    <div class="inputs">
                        <div class="input-left">
                            <label>Email</label>
                            <input type='text' name='email' placeholder='Email'>
                        </div>
                        <div class="input-right">
                            <label>Address</label>
                            <input type='text' name='address' placeholder='Address to deliver dumpster'>
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input-left">
                            <label>City</label>
                            <input type='text' name='city' placeholder='City'>
                        </div>
                        <div class="input-right">
                            <label>Zip</label>
                            <input type='text' name='zip' placeholder='Zip'>
                        </div>
                    </div>
                    <div class="select">
                        <label>Dumpster Size</label>
                        <select name='day' class='yards'>
                            <option selected='selected' value='-1'>Please select</option>
                            <option value='1'>10 Yards</option>
                            <option value='2'>15 Yards</option>
                            <option value='3'>20 Yards</option>
                            <option value='4'>30 Yards</option>
                            <option value='5'>40 Yards</option>
                        </select>
                    </div>

                    <div class="inputs">
                        <div class="input-left">
                            <label>Debris type</label>
                            <input type='text' name='Debris type' placeholder='Debris type'>
                        </div>
                        <div class="input-right">
                            <label>Rental Period</label>
                            <input type='text' name='Rental period' placeholder='Rental period'>
                        </div>
                    </div>
                    <div class='submit_button'><button type='submit'>Submit</button>
                        <h3>Call Us: 303-834-3197</h3>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
