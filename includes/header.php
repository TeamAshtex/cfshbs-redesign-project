<div class='header'>
    <div class='container'>
        <div class='logo-main'>
            <a href='#'><img src='<?php echo BASE_URL; ?>/images/HBS-logo.png' class='logo'></a>
        </div>
        <div class='nav-bar'>
            <ul class='nav-bar-ul'>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/index.php' class='nav-bar-anchor'>HOME</a></li>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/about_us.php' class='nav-bar-anchor'>ABOUT US</a></li>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/junk_removal.php' class='nav-bar-anchor'>JUNK REMOVAL</a></li>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/dumpster_sizes.php' class='nav-bar-anchor'>DUMPSTER SIZES</a></li>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/contact_us.php' class='nav-bar-anchor'>CONTACT</a></li>
                <li class='nav-bar-li'><a href='<?php echo BASE_URL;?>/login.php' class='nav-bar-anchor'>LOGIN</a></li>
            </ul>
        </div>
    </div>
</div>

<div class='slider-position'>
    <!-- Start WOWSlider.com BODY section -->
    <div id='wowslider-container1'>
        <div class='ws_images'><ul>
                <li>
                    <img src='<?php echo BASE_URL; ?>/data1/images/cfhbscoloradodumpsterss3.jpg' alt='cfhbs-colorado-dumpster-ss3' title='cfhbs-colorado-dumpster-ss3' id='wows1_0' />
                </li>
                <li>
                    <a href='http://wowslider.net'>
                        <img src='<?php echo BASE_URL; ?>/data1/images/cfhbscoloradodumpsterss1.jpg' alt='wowslider.net' title='cfhbs-colorado-dumpster-ss1' id='wows1_1'/>
                    </a>
                </li>
                <li>
                    <img src='<?php echo BASE_URL; ?>/data1/images/cfhbscoloradodumpsterss2.jpg' alt='cfhbs-colorado-dumpster-ss2' title='cfhbs-colorado-dumpster-ss2' id='wows1_2'/>
                </li>
            </ul>
        </div>
        <div class='ws_script' style='position:absolute;left:-99%'><a href='http://wowslider.com'>http://wowslider.com/</a> by WOWSlider.com v8.7</div>
        <div class='ws_shadow'></div>
    </div>	
    <script type='text/javascript' src='<?php echo BASE_URL; ?>/engine1/wowslider.js'></script>
    <script type='text/javascript' src='<?php echo BASE_URL; ?>/engine1/script.js'></script>
    <!-- End WOWSlider.com BODY section -->
    <div class='slider-wth-box'>
        <h2>Welcome to HBS</h2>
        <p>We have been supplying Colorado homeowners and construction sites with rental dumpsters and roll off services used for the removal of construction debris, bulk trash and old junk for over 25 years.</p>
        <div class='phone-num'>
            <h3 class='tel'>CALL: 303-834-3115 <span>for Free Estimate.</span></h3>

        </div>
        <a href='#'>READ MORE</a>
    </div>
    <div class='get get_a_quote'>
        <a href='#'>GET A QUOTE</a>
    </div>
</div>
