<?php

echo "    <div class='popup-file'>
                <div class='container'>
                    <div class='popup-bg'></div>
                    <div class='popup-main'>
                        <div class='popup-content'>
                            <a href='#'><img src='images/close_icon.gif' class='close-button'></a>
                            <h4>Helpful info:</h4>
                            <p>Need the dumpster for 3 days or less? $25 OFF FOR 3 DAY OR LESS RENTALS</p>
                            <p>No electronics or hazardous material please.</p>
                            <p>Requested delivery date</p>
                            <form>
                                <select name='month'>
                                    <option selected='selected' value='-1'>Month</option>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                    <option value='4'>4</option>
                                    <option value='5'>5</option>
                                    <option value='6'>6</option>
                                    <option value='7'>7</option>
                                    <option value='8'>8</option>
                                    <option value='9'>9</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>
                                </select>
                                <select name='day'>
                                    <option selected='selected' value='-1'>Day</option>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                    <option value='4'>4</option>
                                    <option value='5'>5</option>
                                    <option value='6'>6</option>
                                    <option value='7'>7</option>
                                    <option value='8'>8</option>
                                    <option value='9'>9</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>
                                    <option value='13'>13</option>
                                    <option value='14'>14</option>
                                    <option value='15'>15</option>
                                    <option value='16'>16</option>
                                    <option value='17'>17</option>
                                    <option value='18'>18</option>
                                    <option value='19'>19</option>
                                    <option value='20'>20</option>
                                    <option value='21'>21</option>
                                    <option value='22'>22</option>
                                    <option value='23'>23</option>
                                    <option value='24'>24</option>
                                    <option value='25'>25</option>
                                    <option value='26'>26</option>
                                    <option value='27'>27</option>
                                    <option value='28'>28</option>
                                    <option value='29'>29</option>
                                    <option value='30'>30</option>
                                    <option value='31'>31</option>
                                </select>
                                <select name='year' class='year'>
                                    <option selected='selected' value='-1'>Year</option>
                                    <option value='1'>2016</option>
                                    <option value='2'>2017</option>
                                    <option value='3'>2018</option>
                                </select>
                                <input type='text' name='name' placeholder='Name'>
                                <input type='text' name='phone' placeholder='Phone (for fastest service)'>
                                <h5>Please Call Me</h5>
                                <input type='checkbox' name='vehicle' class='check_box'>
                                <input type='text' name='email' placeholder='Email'>
                                <input type='text' name='address' placeholder='Address to deliver dumpster'>
                                <input type='text' name='city' placeholder='City'>
                                <input type='text' name='zip' placeholder='Zip'>
                                <h5>Dumpster Size</h5>
                                <select name='day' class='yards'>
                                    <option selected='selected' value='-1'>Please select</option>
                                    <option value='1'>10 Yards</option>
                                    <option value='2'>15 Yards</option>
                                    <option value='3'>20 Yards</option>
                                    <option value='4'>30 Yards</option>
                                    <option value='5'>40 Yards</option>
                                </select>
                                <input type='text' name='Debris type' placeholder='Debris type'>
                                <input type='text' name='Rental period' placeholder='Rental period'>
                                <div class='submit_button'><button type='submit'>Submit</button>
                                    <h3>Call Us: 303-834-3197</h3>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>";
