<div class='why-hair-us_main'>
        <div class='container'>
            <h3>Why Hire Us</h3>
            <p>With over twenty years in the business, thousands of satisfied customers and the lowest dumpster rental pricing in Colorado, the question is 'Why wouldn't you do business with us?'.</p>
            <div class='video-iframe_main'>
            <div class='video-iframe2'>
                <iframe src='https://www.youtube.com/embed/w5XaPE5ZB_Q' frameborder='0' allowfullscreen></iframe> 
            </div>
            <div class='video-iframe2'>
                <iframe src='https://www.youtube.com/embed/rQnfjGAJLFg' frameborder='0' allowfullscreen></iframe>
            </div>
            <div class='video-iframe2'>
                <iframe src='https://www.youtube.com/embed/w5XaPE5ZB_Q' frameborder='0' allowfullscreen></iframe> 
            </div>
            </div>
            <div class='why-hair-us-contant'>
                <h4>HBS Rental Dumpster Pickup</h4>
                <ul>
                    <li><img src='<?php echo BASE_URL;?>/images/HBS-WHS01.png'></li>
                    <li><img src='<?php echo BASE_URL;?>/images/HBS-WHS02.png'></li>
                    <li><img src='<?php echo BASE_URL;?>/images/HBS-WHS03.png'></li>
                    <li><img src='<?php echo BASE_URL;?>/images/HBS-WHS04.png'></li>
                </ul>
            </div>
            <h4 class='call-us'>CALL US OR FILL OUT OUR EASY ONLINE FORM AND WE WILL DELIVER YOUR DUMPSTER TODAY. 303-834-3197</h4>
        </div>
    </div>
