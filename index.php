<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <?php
    require_once 'includes/head.php';
    ?>
    <body>

        <div class="wapper">

            <!-- header -->
            <?php require_once 'includes/header.php'; ?>

            <!-- banner -->
            <!--
            <div class="main-banner">
                <div class="container-banner">
                <img src="images/HBS-banner.png">
                </div>
            </div>
            -->


            <!-- content -->
            <div class="contant">
                <div class="container">
                    <div class="contant-blocks">
                        <h3>Residential Roll Off Dumpster</h3>
                        <p>Refinishing a basement, bathroom or kitchen or maybe just cleaning out the garage? Maybe you just finished putting on a new roof yourself and need a roll off container for the debris. If you have the need to get rid of trash, wood, shingles, etc.., we have the right size roll off dumpster to suite your needs.</p>
                        <a href="#">Enter</a>
                    </div>
                    <div class="contant-blocks">
                        <h3>Commercial Roll Off Services</h3>
                        <p>How about a mix of roll off sizes spread out over a fairly large geographic area? Need your roll off dumpsters delivered in a hurry? With over 25 years of experience in the industry we can handle just about any roll off container need you may have.</p>
                        <a href="#">Enter</a>
                    </div>
                </div>
            </div>
            <!-- services -->

            <div class="about-services">
                <div class="container border-bottom">
                    <div class="about-services-contant">
                        <h3>Fast, Reliable and Affordable</h3>
                        <p>We provide fast, reliable affordable dumpster rentals. We've been providing local roll off services for over twenty years. When you need to remove debris or junk, you need a reliable partner. When you call us to rent a dumpster you get that dumpster when you need it and when you need that rental dumpster removed it gets removed on time. When you rent a dumpster from us you will get the very best dumpster rental price in Colorado. We've been in this business for a very long time and nobody can match our roll off services or dumpster rental pricing</p>
                        <ul>
                            <li><img src="images/HBS-errow_06.png">Same Day or Next Day Service</li>
                            <li><img src="images/HBS-errow_06.png">No hidden fees</li>
                            <li><img src="images/HBS-errow_06.png">Locally owned and operated since 1980</li>
                            <li><img src="images/HBS-errow_06.png">Prompt delivery and pick up</li>
                            <li><img src="images/HBS-errow_06.png">Professional representative</li>
                            <li><img src="images/HBS-errow_06.png">Professional representative</li>
                            <li><img src="images/HBS-errow_06.png">Ordering available 7:00AM - 5:00PM M-F</li>
                        </ul>
                    </div>
                    <div class="about-services-video">

                        <iframe class="video-iframe" src="https://www.youtube.com/embed/rQnfjGAJLFg" frameborder="0" allowfullscreen></iframe>
                        <a href="#" class="get_a_quote">GET FREE QUOTE</a>
                    </div>
                </div>
            </div>
            <!-- yards block -->

            <div class="yards-block">
                <div class="container">
                    <ul>
                        <li>
                            <img src="images/HBS-10yard.png">
                            <h4>10 yard</h4>
                            <h4>Front Load Container</h4>
                            <p>13'L X 8'W X 4.5'H</p>
                        </li>
                        <li>
                            <img src="images/HBS-15yard.png">
                            <h4>15 yard</h4>
                            <h4>Front Load Container</h4>
                            <p>13'L X 8'W X 4.5'H</p>
                        </li>
                        <li>
                            <img src="images/HBS-20yard.png">
                            <h4>20 yard</h4>
                            <h4>Front Load Container</h4>
                            <p>13'L X 8'W X 4.5'H</p>
                        </li>
                        <li>
                            <img src="images/HBS-30yard.png">
                            <h4>30 yard</h4>
                            <h4>Front Load Container</h4>
                            <p>13'L X 8'W X 4.5'H</p>
                        </li>
                        <li>
                            <img src="images/HBS-40yard.png">
                            <h4>40 yard</h4>
                            <h4>Front Load Container</h4>
                            <p>13'L X 8'W X 4.5'H</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- add -->
            <?php require_once 'includes/hbs_add.php'; ?>
            <!-- why hair us -->

            <div class="why-hair-us">
                <div class="container">
                    <h3>Why Hire Us</h3>
                    <div class="video-iframe2">
                        <iframe src="https://www.youtube.com/embed/w5XaPE5ZB_Q" frameborder="0" allowfullscreen></iframe> 
                    </div>
                    <div class="why-hair-us-contant">
                        <p>With over twenty years in the business, thousands of satisfied customers and the lowest dumpster rental pricing in Colorado, the question is "Why wouldn't you do business with us?".</p>
                        <h4>HBS Rental Dumpster Pickup</h4>
                        <ul>
                            <li><img src="images/HBS-WHS01.png"></li>
                            <li><img src="images/HBS-WHS02.png"></li>
                            <li><img src="images/HBS-WHS03.png"></li>
                            <li><img src="images/HBS-WHS04.png"></li>
                        </ul>
                    </div>
                    <h4 class="call-us">CALL US OR FILL OUT OUR EASY ONLINE FORM AND WE WILL DELIVER YOUR DUMPSTER TODAY. 303-834-3197</h4>
                </div>
            </div>
            <!-- map and cities -->
            <?php require_once 'includes/cities_and_address.php'; ?>
            <!-- footer -->
            <?php require_once 'includes/footer.php'; ?>
        </div>    
    </body>
</html>
