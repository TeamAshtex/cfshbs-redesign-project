$(document).ready(function() {
    $('body').delegate('.get_a_quote', 'click', function() {
        $('.popup-file').show();
    });
    $('body').delegate('.close_get_a_quote', 'click', function() {
        $('.popup-file').hide();
    });
    $('.cities').on('click', function() {
        url = window.location.href;
        city = $(this).find('.city').html();
        zip = $(this).find("#zip").val();
        var a = document.createElement('a');
        a.href = url;
        var rm = a.pathname.split('/')[2];
        if (rm) {
            rm = 'cities.php';
            window.location.href = rm + '?city=' + city + '&zip=' + zip;
        }
        else {
            window.location.href = url + 'cities.php?city=' + city + '&zip=' + zip;
        }
    });
});

