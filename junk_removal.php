<?php require_once 'includes/head.php'; ?>
<div class="wapper">
    <?php require_once 'includes/header.php' ?>
    <div class="page_name">
        <div class="container">
            <h2>Junk Removal</h2>
        </div>
    </div>
    <?php require_once 'includes/main_buttons.php'; ?>
    <div class="junk_removal_contant">
        <div class="container">
            <p>We have been providing low cost roll off dumpster rental service for junk removal or bulk trash removal projects to our homeowner and business owner customers for over 25 years. If you have a project that requires the removal of construction debris, bulk trash, old junk or other building debris, we can deliver roll off dumpsters of the appropriate size. We provide roll off dumpster services to the entire front range including Denver, Boulder, Longmont, Loveland, Fort Collins and the entire Northern Colorado area.</p>
            <p>When you have construction debris, need to haul off a load of junk or have bulk trash that needs to be removed, we can supply an affordable roll off dumpster of your desired size. Our temporary roll off dumpsters come in 20 yard, 30 yard or 40 yard sizes. Our dumpsters can handle your bulk trash items as well as massive amounts of construction and other debris. When you call we will deliver your dumpsters on time, every time. Once your temporary dumpster is in place you can load it yourself and we'll pick it up.</p>
        </div>
    </div>
    <?php require_once 'includes/hbs_add.php' ?>
    <?php require_once 'includes/why_hire_us.php' ?>

    <div class="junkmap">
        <div class="container">
        <h3>Local Service Area</h3>
        <p>To schedule delivery of your temporary roll off dumpster or front load container in Arapahoe, CO please call 303-834-3197.</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13602.112635610712!2d74.39300511458737!3d31.537116858315166!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1490618984799" allowfullscreen></iframe>

        </div>
    </div>

    <?php require_once 'includes/footer.php' ?>;
</div>
