<?php require_once 'includes/head.php'; ?>
<div class="wapper">
    <div class="login-page">
        <div class="container">
            <div class="login-box">
                <img src="images/header_logo2.jpg">
                <h2>Welcome To Custom Fence</h2>
                <form>
                    <h4>Email:</h4>
                    <input type="text" name="Email" placeholder="Email">
                    <h4>Your password:</h4>
                    <input type="password" name="Your password" placeholder="Your password">
                </form>
                <div class="login-button">
                    <button type="submit" name="login">Login</button>
                </div>
                <div class="click-here">
                    <p>Forgot You Password <a href="forget_password.php">Click here</a></p>
                    <p>Don't have an account??? <a href="signup.php">Click here</a> to sign up.</p>
                    <p><a href="index.php">Go Back to Homepage</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
