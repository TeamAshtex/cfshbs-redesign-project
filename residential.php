<?php require_once 'includes/head.php'; ?>
<div class="wapper">
    <?php require_once 'includes/header.php' ?>
    <div class="page_name">
        <div class="container">
            <h2>Residential Dumpster Rentals</h2>
        </div>
    </div>
    <?php require_once 'includes/main_buttons.php'; ?>
    <?php require_once 'includes/hbs_add.php' ?>
    <div class="residenrial_main">
        <div class="container">
            <h3>Residential Roll Off Dumpster Rental / Trash Container Rentals</h3>
            <p>Refinishing a basement, bathroom or kitchen or maybe just cleaning out the garage? Maybe you just finished putting on a new roof yourself and need a roll off container for the debris. If you have the need to get rid of trash, wood, shingles, etc.., we have the right size roll off dumpster to suite your needs.</p>
            <p>No matter what your remodeling, renovation or cleanup project is we can supply the roll-offs and dumpsters to suit your needs. From garage cleanup to basement cleanout or yard / property cleanup or even structure renovations, having us deliver your roll off dumpster will be fast, affordable, timely and professional. Also, we're a local company having started here in Colorado more than 25 years ago. So, you can feel confident that we will deliver the service you need, when you need it.</p>
        </div>
        <div class="examples_and_contact">
            <div class="container">
                <div class="examples">
                    <h3>Examples:</h3>
                    <ul>
                        <li>Roofing waste</li>
                        <li>Drywall</li>
                        <li>Shingles</li>
                        <li>Flooring materials</li>
                        <li>Lumber and hardscape materials</li>
                        <li>Household waste and junk disposal</li>
                        <li>Like wood, metal</li>
                        <li>Appliances</li>
                        <li>Spring cleaning & garage / basement cleanouts</li>
                        <li>Green waste</li>
                        <li>Like grass clippings</li>
                        <li>Tree limbs</li>
                        <li>Brush and seasonal landscape cleanups</li>
                        
                    </ul>
                </div>
                <div class="contact">
                    <h3>Contact:</h3>
                    <p>Please feel free to give us a call and we'd be happy to discuss your needs with you.</p>
                    <p>Call us at <strong>303-834-3197</strong> to talk to schedule a delivery of one of our roll off dumpsters!</p>
                    <p>$25 Weekender Discount for Friday Delivery / Monday Pickup Customers!</p>
                </div>
            </div>
        </div>
    </div>
    <?php require_once 'includes/why_hire_us.php' ?>
    <?php require_once 'includes/footer.php' ?>;
</div>
